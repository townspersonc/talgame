﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (!_instance)
            {
                HandleInstanceCreation();
            }

            return _instance;
        }
    }

    protected void Awake()
    {
        if (!_instance) HandleInstanceCreation();


        if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    private static void HandleInstanceCreation()
    {
        _instance = (T) FindObjectOfType(typeof(T));

        if (_instance == null)
        {
            // Need to create a new GameObject to attach the singleton to.
            GameObject singletonObject = new GameObject();
            _instance = singletonObject.AddComponent<T>();
            singletonObject.name = typeof(T).ToString();
        }

        if (Application.isPlaying)
        {
            DontDestroyOnLoad(_instance);
        }
    }
}