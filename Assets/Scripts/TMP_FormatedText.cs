﻿using TMPro;
using UnityEngine;

public class TMP_FormatedText : MonoBehaviour
{
    [SerializeField] private TMP_Text m_Text = null;
    private string m_Format = string.Empty;

    public string Text => m_Text.text;

    public void SetText(string i_Text)
    {
        FormatCheck();
        m_Text.text = i_Text;
    }

    public void SetText(float i_Val)
    {
        FormatCheck();

        m_Text.SetText(m_Format, i_Val);
    }

    public void SetText(float i_1, float i_2)
    {
        FormatCheck();

        m_Text.SetText(m_Format, i_1, i_2);
    }

    private void FormatCheck()
    {
        if (m_Format == string.Empty) m_Format = m_Text.text;
    }


    private void Reset()
    {
        GetRefs();
    }

    private void GetRefs()
    {
        m_Text = GetComponent<TMP_Text>();
        if (m_Text) m_Format = m_Text.text;
    }
}