﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PoolManager : MonoBehaviour
{
    [SerializeField] private List<PoolData> data = new List<PoolData>();

    private void Start()
    {
        PopulateAll();
    }

    public GameObject Dequeue(PoolType type)
    {
        GameObject result = null;

        foreach (var d in data)
        {
            if (d.Type == type)
            {
                if (d.Objects.Count > 0)
                {
                    result = d.Objects[0];
                    d.Objects.RemoveAt(0);

                    break;
                }

                result = Instantiate(d.Prefab, d.Parent);
                break;
            }
        }

        result.Enable();

        return result;
    }

    public void Queue(PoolType type, GameObject obj)
    {
        foreach(var d in data)
        {
            if(d.Type == type)
            {
                d.Objects.Add(obj);
                obj.transform.SetParent(d.Parent);
                obj.Disable();
            }
        }
    }

    [ContextMenu("Populate")]
    private void ForcePopulate()
    {
        data.ForEach(d => d.Objects.Clear());
        PopulateAll();
    }

    private void PopulateAll()
    {
        foreach (PoolData d in data)
        {
            if (d.Objects == null || !d.Parent || ( d.Parent && d.Parent.childCount < d.DefaultAmount))
            {
                Transform t = transform.Find(d.Type.ToString());
                if (!t)
                {
                    t = new GameObject(d.Type.ToString()).transform;
                    t.SetParent(transform);
                }

                t.DestroyAllChildren();
                d.Parent = t;

                d.Objects = new List<GameObject>();

                GameObject g = null;
                for (int i = 0; i < d.DefaultAmount; i++)
                {
                    g = null;
                    if(Application.isPlaying)
                    {
                        g = Instantiate(d.Prefab);
                    }
#if UNITY_EDITOR
                    else
                    {
                        g = PrefabUtility.InstantiatePrefab(d.Prefab) as GameObject;
                    }
#endif

                    g.transform.SetParent(d.Parent);
                    g.SetActive(false);
                    d.Objects.Add(g);
                }
            }
        }
    }

    [System.Serializable]
    private class PoolData
    {
        public PoolType Type = default;
        public int DefaultAmount = 5;
        public GameObject Prefab = null;
        [HideInInspector] public List<GameObject> Objects = new List<GameObject>();
        [HideInInspector] public Transform Parent = null;
    }
}

public enum PoolType
{
    Duck,
    Bucket,
    ProgressDot,
    Smoke,
    CorrectParticle,
    CategoryButton,
    CategoryPageButton
}
