﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace SplitDuck
{
    public class Fragment : MonoBehaviour
    {
        [SerializeField] private SortingGroup m_SortingGroup = null;
        [SerializeField] private SpriteRenderer m_OutLine = null;
        [SerializeField] private Transform m_ShaterPosition = null;
        [SerializeField] private Collider2D m_Collider = null;

        private Vector3 m_SnapPos;

        private static FragmentSettings Settings => Manager.Instance.Settings.FragmentSettings;

        public bool Snapped => !m_Collider.enabled;
        public bool DragMode { get; private set; }

        public static Action OnSnap;

        private void Start()
        {
            m_SnapPos = transform.position;
        }

        private void OnMouseDown()
        {
            if (!DragMode)
            {
                EnterDragMode();
            }
        }

        private void OnMouseUp()
        {
            if (DragMode)
            {
                ExitDragMode();
            }
        }

        private void Update()
        {
            DragLoop();
        }

        private void DragLoop()
        {
            if (!DragMode) return;

            transform.GainXY(Vector3.MoveTowards(transform.position, Manager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition), Settings.DragSpeed * Time.deltaTime));
        }

        private void EnterDragMode()
        {
            DragMode = true;
            m_SortingGroup.sortingOrder = Settings.DraggingSortingOrder;
        }

        private void ExitDragMode()
        {
            DragMode = false;
            m_SortingGroup.sortingOrder = Settings.ShatteredSortingOrder;

            if(Vector2.Distance(transform.position, m_SnapPos) < Settings.SnapDiscante)
            {
                GetSnapped();
            }
        }

        private void GetSnapped()
        {
            m_Collider.enabled = false;
            m_SortingGroup.sortingOrder = Settings.SnappedSortingOrder;
            transform.DOMove(m_SnapPos, Settings.SnapTween.Time).SetEase(Settings.SnapTween.Ease).OnComplete(() => OnSnap?.Invoke());
            transform.DOScale(1f, Settings.SnapTween.Time).SetEase(Settings.SnapTween.Ease);
            m_OutLine.DOFade(0f, Settings.OutlineTween.Time).SetEase(Settings.OutlineTween.Ease);
        }

        public void Shatter()
        {
            transform.DOMove(m_ShaterPosition.position, Settings.ShaterMoveTween.Time).SetEase(Settings.ShaterMoveTween.Ease).OnComplete(() =>
            {
                m_Collider.enabled = true;
            });
            transform.DOScale(Settings.ShaterScaleTween.Value, Settings.ShaterScaleTween.Time).SetEase(Settings.ShaterScaleTween.Ease);
            m_OutLine.DOFade(1f, Settings.OutlineTween.Time).SetEase(Settings.OutlineTween.Ease);
        }

        private void Reset()
        {
            m_SortingGroup = GetComponent<SortingGroup>();
            m_Collider = GetComponent<Collider2D>();
        }
    }

    [System.Serializable]
    public class FragmentSettings
    {
        public float SnapDiscante = 2f;
        public float DragSpeed = 300f;
        public TweenDataBase ShaterMoveTween;
        public TweenDataBase OutlineTween;
        public FloatTweenData ShaterScaleTween;
        public TweenDataBase SnapTween;

        public int SnappedSortingOrder = 0;
        public int ShatteredSortingOrder = 1;
        public int DraggingSortingOrder = 2;
    }
}
