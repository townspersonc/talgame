﻿using System.Collections.Generic;
using UnityEngine;

namespace SplitDuck
{
    public class PositionShuffler : MonoBehaviour
    {
        [SerializeField] private List<Transform> transforms = new List<Transform>();
        private List<Vector3> positions;

        private void OnEnable()
        {
            Manager.OnComplete += Shuffle;
        }

        private void OnDisable()
        {
            Manager.OnComplete -= Shuffle;
        }

        private void Start()
        {
            positions = new List<Vector3>();

            foreach (Transform i in transform)
            {
                positions.Add(i.position);
            }
        }

        private void Shuffle()
        {
            positions.Shuffle();

            for (int i = 0; i < transforms.Count; i++)
            {
                transforms[i].position = positions[i];
            }
        }

        private void Reset()
        {
            transforms = new List<Transform>();

            for (int i = 0; i < transform.childCount; i++)
            {
                transforms.Add(transform.GetChild(i));
            }
        }
    }
}