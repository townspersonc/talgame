﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SplitDuck
{
    public class Manager : Singleton<Manager>
    {
        public Camera MainCamera;
        [SerializeField] private List<Fragment> m_Fragments = new List<Fragment>();

        public Settings Settings;

        public static Action OnComplete;

        private void OnEnable()
        {
            Fragment.OnSnap += OnCompleteCheck;
        }

        private void OnDisable()
        {
            Fragment.OnSnap -= OnCompleteCheck;
        }

        private void OnCompleteCheck()
        {
            if (m_Fragments.Any(f => !f.Snapped)) return;

            OnComplete?.Invoke();
        }

        private void Reset()
        {
            MainCamera = Camera.main;
        }

        public void Shatter()
        {
            m_Fragments.ForEach(f => f.Shatter());
        }
    }

    [System.Serializable]
    public class Settings
    {
        public FragmentSettings FragmentSettings;
        public TweenDataInOut ShatterButtonTween;
    }
}
