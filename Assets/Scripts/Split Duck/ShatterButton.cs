﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace SplitDuck
{
    public class ShatterButton : MonoBehaviour
    {
        [SerializeField] private Transform m_Root = null;
        [SerializeField] private Button m_Button = null;

        public static TweenDataInOut TweenData => Manager.Instance.Settings.ShatterButtonTween;

        private Tween tween;

        private void OnEnable()
        {
            m_Button.onClick.AddListener(OnButtonClick);
            Manager.OnComplete += Appear;
        }

        private void OnDisable()
        {
            m_Button.onClick.RemoveListener(OnButtonClick);
            Manager.OnComplete -= Appear;
        }

        private void OnButtonClick()
        {
            Manager.Instance.Shatter();
            Hide();
        }

        private void Appear()
        {
            if (tween.IsActive()) tween.Kill();

            m_Button.interactable = true;
            tween = m_Root.DOScale(1f, TweenData.In.Time).SetEase(TweenData.In.Ease);
        }

        private void Hide()
        {
            if (tween.IsActive()) tween.Kill();

            m_Button.interactable = false;
            tween = m_Root.DOScale(0f, TweenData.Out.Time).SetEase(TweenData.Out.Ease);
        }

        private void Reset()
        {
            m_Button = GetComponentInChildren<Button>();
        }
    }
}
