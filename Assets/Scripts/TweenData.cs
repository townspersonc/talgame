﻿using DG.Tweening;
using UnityEngine;

[System.Serializable]
public class TweenDataBase
{
    public float Time = 0.25f;
    public Ease Ease = Ease.OutBack;
}

[System.Serializable]
public class FloatTweenData : TweenDataBase
{
    public float Value = 0.5f;
}

[System.Serializable]
public class IntTweenData : TweenDataBase
{
    public int Value = 1;
}

[System.Serializable]
public class TweenDataInOut
{
    public TweenDataBase In;
    public TweenDataBase Out;
    public float FullTime => In.Time + Out.Time;
}

[System.Serializable]
public class TweenDataFloatInOut : TweenDataInOut
{
    public float Value;
}

[System.Serializable]
public class Vector3TweenData : TweenDataBase
{
    public Vector3 Value = Vector3.up;
}

[System.Serializable]
public class CurveTweenData : TweenDataBase
{
    public AnimationCurve Curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
}

[System.Serializable]
public class CurveAndFloatTweenData : CurveTweenData
{
    public float Value;
}