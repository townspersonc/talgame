﻿using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Lobby
{
    public class LobbyScreen : MenuScreen
    {
        [SerializeField] private Transform m_CategoriesRoot = null;
        [SerializeField] private Transform m_PageButtonsRoot = null;
        [SerializeField] private Button m_LoadAllButton = null;

        private List<CategoryButton> m_Categories = new List<CategoryButton>();

        private Tween m_LoadTween;
        private int m_LoadedPageID = -1;

        public static Settings Settings => Lobby.Instance.Settings;
        public static LobbyScreenParameters Params => Lobby.Instance.Settings.LobbyScreenParameters;

        private void OnEnable()
        {
            m_LoadAllButton.onClick.AddListener(LoadAll);    
        }
        private void OnDisable()
        {
            m_LoadAllButton.onClick.RemoveListener(LoadAll);
        }

        public void Init()
        {
            foreach (var i in Settings.Pages)
            {
                PageButton.Init(m_PageButtonsRoot, this, i.ID);
            }

            Open();
            DOVirtual.DelayedCall(OpenTime + Params.CategoriesLoadOpenDelay, LoadAll);
        }

        public override void Open()
        {
            base.Open();

            if(m_Categories.Count > 0)
            {
                ReShowCategories(OpenTime);
            }
        }

        public void LoadAll() => LoadPage(Settings.AllCategories);

        public void OnPageButtonClick(int i_ID)
        {
            var data = Settings.Pages.FirstOrDefault(p => p.ID == i_ID);

            if (data == null) return;

            LoadPage(data);
        }

        private void LoadPage(PageData i_PageData)
        {
            if (m_LoadedPageID == i_PageData.ID) return;
            else m_LoadedPageID = i_PageData.ID;

            if (m_LoadTween.IsActive()) m_LoadTween.Kill(); 

            float loadDelay = 0f;

            if(m_Categories.Count > 0)
            {
                foreach (var m in m_Categories)
                {
                    m.Finalise();
                    if (m.HideTime > loadDelay) loadDelay = m.HideTime;
                }

                loadDelay += Params.CategoriesReloadDelay;
                m_Categories.Clear();
            }

            m_LoadTween = DOVirtual.DelayedCall(loadDelay, () =>
            {
                for(int i = 0; i < i_PageData.Categories.Count; i++)
                {
                    m_Categories.Add(CategoryButton.Init(m_CategoriesRoot, i_PageData.Categories[i], i * Params.CategoriesLoadStep));
                }
            });
        }

        private void ReShowCategories(float i_Delay = 0f)
        {
            for(int i = 0; i < m_Categories.Count; i++)
            {
                m_Categories[i].Appear(i_Delay + i * Params.CategoriesLoadStep);
            }
        }
    }

    [System.Serializable]
    public class LobbyScreenParameters
    {
        public float CategoriesLoadOpenDelay = 0.5f;
        public float CategoriesLoadStep = 0.15f;
        public float CategoriesReloadDelay = 0.2f;
    }
}
