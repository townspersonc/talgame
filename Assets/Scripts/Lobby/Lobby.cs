﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lobby
{

    public class Lobby : Singleton<Lobby>
    {
        [SerializeField] private LobbyScreen m_LobbyScreen = null;
        public PoolManager pool;

        public Settings Settings;

        private Category m_ActiveCategorie = null;

        private void Start()
        {
            m_LobbyScreen.Init();
        }

        public void Load(Category i_Category)
        {
            m_ActiveCategorie = i_Category;
            SceneManager.LoadScene(i_Category.SceneBuildID, LoadSceneMode.Additive);

            m_LobbyScreen.Close();
        }

        public void BackToLobby()
        {
            m_LobbyScreen.Open();
            SceneManager.UnloadSceneAsync(m_ActiveCategorie.SceneBuildID);
        }
    }

    [System.Serializable]
    public class Settings
    {
        public PageData AllCategories;
        public List<PageData> Pages = new List<PageData>();
        public LobbyScreenParameters LobbyScreenParameters;
    }


    [System.Serializable]
    public class PageData
    {
        public int ID;
        public List<Category> Categories;
    }
}
