﻿using UnityEngine;

namespace Lobby
{
    public class BackToLobbyButton : MonoBehaviour
    {
        [SerializeField] private OnceButton m_Button = null;

        private void OnEnable()
        {
            m_Button.onClick.AddListener(GoToLobby);
        }
        private void OnDisable()
        {
            m_Button.onClick.RemoveListener(GoToLobby);
        }

        private void GoToLobby()
        {
            Lobby.Instance.BackToLobby();
        }

        private void Reset()
        {
            m_Button = GetComponent<OnceButton>();
        }
    }
}