﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(Category), menuName = "Scriptable Objects/" + nameof(Category))]
public class Category : ScriptableObject
{
    public Sprite Sprite;
    public string Name;
    public int ID;
    public int SceneBuildID;
}