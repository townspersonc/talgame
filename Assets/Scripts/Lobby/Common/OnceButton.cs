﻿using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnceButton : Button
{
    public bool Clicked { get; private set; }

    protected override void OnEnable()
    {
        Clicked = false;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (Clicked) return;
        Clicked = true;
        base.OnPointerUp(eventData);
    }
}