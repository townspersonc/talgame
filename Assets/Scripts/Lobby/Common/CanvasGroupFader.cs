﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasGroupFader : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_Group = null;
    [SerializeField] private float m_TargetAlpha = 0f;
    [SerializeField] private float m_Time = 1f;
    [SerializeField] private float m_Delay = 0f;
    [SerializeField] private bool m_StartOnEnable = false;

    private void OnEnable()
    {
        if (m_StartOnEnable) DoFade();
    }

    public void DoFade()
    {
        DoFade(m_TargetAlpha, m_Time, m_Delay);
    }

    private void DoFade(float i_Alpha, float i_Time, float i_Delay)
    {
        DOTween.Kill(m_Group);
        m_Group.DOFade(i_Alpha, i_Time).SetDelay(i_Delay);
    }

    private void Reset()
    {
        GetGroup();
    }

    [ContextMenu("Find Reference")]
    private void GetGroup()
    {
        m_Group = GetComponent<CanvasGroup>();
    }
}
