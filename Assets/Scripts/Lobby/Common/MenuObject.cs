﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MenuObject : MonoBehaviour
{
    [SerializeField]
    private bool m_WorkOnEnable = true;
    [SerializeField]
    private AnimationData m_ShowAnimation;
    [SerializeField]
    private AnimationData m_HideAnimation;
    private Tweener m_Tween;
    private DefaultData m_DefaultData;

    public float FullTimeShow { get { return m_ShowAnimation.Time + m_ShowAnimation.Delay; } }
    public float FullTimeHide { get { return m_HideAnimation.Time + m_HideAnimation.Delay; } }

    private void OnEnable()
    {
        if (m_WorkOnEnable)
        {
            Show();
        }
    }
    private void OnDisable()
    {
        m_Tween?.Kill();
    }

    [ContextMenu("Show")]
    public void Show()
    {
        StartTween(true);
    }
    [ContextMenu("Hide")]
    public void Hide()
    {
        StartTween(false);
    }

    private void StartTween(bool i_IsShow)
    {
        if (m_DefaultData == null)
        {
            m_DefaultData = new DefaultData(this);
        }
        m_Tween?.Kill();
        m_Tween = null;
        AnimationData animationData = i_IsShow ? m_ShowAnimation : m_HideAnimation;

        if (i_IsShow)
        {
            m_DefaultData.SetDefaultData();
        }
        if (animationData.Type == AnimationData.eType.Fade)
        {
            if (i_IsShow)
            {
                if (m_DefaultData.Image != null)
                {
                    m_DefaultData.Image.color = new Color(m_DefaultData.Image.color.r, m_DefaultData.Image.color.g, m_DefaultData.Image.color.b, 0);
                    m_Tween = m_DefaultData.Image.DOFade(m_DefaultData.Alpha, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
                }
                else if (m_DefaultData.CanvasGroup != null)
                {
                    m_DefaultData.CanvasGroup.alpha = 0;
                    m_Tween = m_DefaultData.CanvasGroup.DOFade(m_DefaultData.Alpha, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
                }
            }
            else
            {
                if (m_DefaultData.Image != null)
                {
                    m_Tween = m_DefaultData.Image.DOFade(0, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
                }
                else if (m_DefaultData.CanvasGroup != null)
                {
                    m_Tween = m_DefaultData.CanvasGroup.DOFade(0, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
                }

            }
        }
        else if (animationData.Type == AnimationData.eType.MoveAnchor)
        {
            if (i_IsShow)
            {
                m_DefaultData.RectTransform.anchoredPosition = animationData.Position;
                m_Tween = m_DefaultData.RectTransform.DOAnchorPos(m_DefaultData.Position, animationData.Time)
                    .SetEase(animationData.Ease)
                    .SetDelay(animationData.Delay);
            }
            else
            {
                m_Tween = m_DefaultData.RectTransform.DOAnchorPos(animationData.Position, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
            }
        }
        else if (animationData.Type == AnimationData.eType.Scale)
        {
            if (i_IsShow)
            {
                m_DefaultData.RectTransform.localScale = Vector3.zero;
                m_Tween = m_DefaultData.RectTransform.DOScale(m_DefaultData.Scale, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
            }
            else
            {
                m_Tween = m_DefaultData.RectTransform.DOScale(0, animationData.Time).SetEase(animationData.Ease).SetDelay(animationData.Delay);
            }
        }

        if (m_Tween != null)
        {
            m_Tween.SetUpdate(true);
        }
    }



    [System.Serializable]
    private class AnimationData
    {
        public enum eType { Fade, Scale, MoveAnchor }
        public eType Type = eType.Scale;
        public float Time;
        public float Delay;
        public Ease Ease = Ease.Linear;
        public Vector2 Position;
    }

    private class DefaultData
    {
        public CanvasGroup CanvasGroup;
        public Image Image;
        public RectTransform RectTransform;

        public float Alpha;
        public Vector2 Position;
        public Vector3 Scale;
        public DefaultData(MenuObject i_MenuObject)
        {
            CanvasGroup = i_MenuObject.GetComponent<CanvasGroup>();
            Image = i_MenuObject.GetComponent<Image>();
            RectTransform = i_MenuObject.GetComponent<RectTransform>();

            if (Image != null)
            {
                Alpha = Image.color.a;
            }
            if (CanvasGroup != null)
            {
                Alpha = CanvasGroup.alpha;
            }


            Scale = RectTransform.localScale;
        }

        public void SetDefaultData()
        {
            if (Image != null)
            {
                Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, Alpha);
            }
            if (CanvasGroup != null)
            {
                CanvasGroup.alpha = Alpha;
            }
            RectTransform.localScale = Scale;
        }
    }
}
