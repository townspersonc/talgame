﻿using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MenuScreen : MonoBehaviour
{
    [SerializeField] protected List<MenuObject> m_MenuObjects = null;

    public float OpenTime { get; private set; }
    public float CloseTime { get; private set; }

    private void Awake()
    {
        if (m_MenuObjects.Count > 0)
        {
            OpenTime = m_MenuObjects.Max(m => (m.FullTimeShow));
            CloseTime = m_MenuObjects.Max(m => (m.FullTimeHide));
        }
    }

    public virtual void Open()
    {
        gameObject.Enable();
    }

    public virtual void Close()
    {
        m_MenuObjects.ForEach(m => { if (m.gameObject.activeInHierarchy) m.Hide(); });

        DOVirtual.DelayedCall(CloseTime, gameObject.Disable);
    }


    private void Reset()
    {
        FindChildMenuObjects();
    }

    [ContextMenu("FindChildMenuObjects")]
    private void FindChildMenuObjects()
    {
        m_MenuObjects = new List<MenuObject>(GetComponentsInChildren<MenuObject>(true));
    }
}
