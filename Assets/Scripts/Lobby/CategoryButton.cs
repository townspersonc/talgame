﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Lobby
{
    public class CategoryButton : MonoBehaviour
    {
        [SerializeField] private OnceButton m_Button = null;
        [SerializeField] private Image m_Image = null;
        [SerializeField] private MenuObject m_MenuObject = null;
        public float HideTime => m_MenuObject.FullTimeHide;
        public Category Category { get; private set; }

        private void OnEnable()
        {
            m_Button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            m_Button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            Lobby.Instance.Load(Category);
        }

        public static CategoryButton Init(Transform i_Parent, Category i_Category, float i_ShowDelay = 0f)
        {
            CategoryButton res = Lobby.Instance.pool.Dequeue(PoolType.CategoryButton).GetComponent<CategoryButton>();
            res.transform.SetParent(i_Parent);
            res.transform.localPosition = Vector3.zero;
            res.Init(i_Category, i_ShowDelay);
            return res;
        }

        public void Init(Category i_Category, float i_ShowDelay = 0f)
        {
            m_Image.sprite = i_Category.Sprite;
            Category = i_Category;
            Appear(i_ShowDelay);
        }

        public void Appear(float i_ShowDelay = 0f)
        {
            transform.localScale = Vector3.zero;
            DOVirtual.DelayedCall(i_ShowDelay, () =>
                {
                    transform.localScale = Vector3.one;
                    m_MenuObject.Show();
                });
        }

        public void Finalise()
        {
            m_MenuObject.Hide();
            DOVirtual.DelayedCall(m_MenuObject.FullTimeHide, () =>
            {
                Lobby.Instance.pool.Queue(PoolType.CategoryPageButton, gameObject);
            });
        }

        private void Reset()
        {
            m_Button = GetComponent<OnceButton>();
            m_Image = GetComponent<Image>();
            m_MenuObject = GetComponent<MenuObject>();
        }
    }
}