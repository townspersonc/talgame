﻿using UnityEngine;
using UnityEngine.UI;

namespace Lobby
{
    public class PageButton : MonoBehaviour
    {
        [SerializeField] private Button m_Button = null;
        [SerializeField] private TMP_FormatedText m_Text = null;

        private LobbyScreen m_Screen = null;
        private int ID;

        private void OnEnable()
        {
            m_Button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            m_Button.onClick.RemoveListener(OnClick);
        }

        public static PageButton Init(Transform i_Parent, LobbyScreen i_Screen, int i_PageID)
        {
            PageButton res = Lobby.Instance.pool.Dequeue(PoolType.CategoryPageButton).GetComponent<PageButton>();

            res.transform.SetParent(i_Parent);
            res.transform.localScale = Vector3.one;
            res.transform.position = Vector3.zero;
            res.Init(i_Screen, i_PageID);

            return res;
        }

        public void Init(LobbyScreen i_Screen, int i_PageID)
        {
            m_Screen = i_Screen;
            ID = i_PageID;
            m_Text.SetText(i_PageID);
        }

        private void OnClick()
        {
            m_Screen.OnPageButtonClick(ID);
        }

        private void Reset()
        {
            m_Button = GetComponent<Button>();
            m_Text = GetComponentInChildren<TMP_FormatedText>();
        }
    }
}