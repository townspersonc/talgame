﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Extentions
{
    #region Math

    /// /// <summary>
    /// Usefull for getting percentagewise values for 2 different ranges. Example 50 from 1 to 100 is 5 from 1 to 10.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="in_min">Current range min</param>
    /// <param name="in_max">Current range max</param>
    /// <param name="out_min">Target range min</param>
    /// <param name="out_max">Target range max</param>
    /// <returns></returns>
    public static float Map(float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (Mathf.Clamp(x, in_min, in_max) - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="integer"></param>
    /// <param name="i_One">Inclusive</param>
    /// <param name="i_Two">Exsclusive</param>
    /// <returns></returns>
    public static bool Between(this int integer, int i_One, int i_Two)
    {
        if (i_One > i_Two) Swap(ref i_One, ref i_Two);

        return integer >= i_One && integer < i_Two;
    }
    public static bool Between(this float f, float i_One, float i_Two)
    {
        if (i_One > i_Two) Swap(ref i_One, ref i_Two);

        return f >= i_One && f < i_Two;
    }

    public static void Swap(ref int x, ref int y)
    {
        x += y;
        y = x - y;
        x -= y;
    }

    public static void Swap(ref float x, ref float y)
    {
        x += y;
        y = x - y;
        x -= y;
    }

    /// <summary>
    /// Returns Modulo as defined in Math.
    /// </summary>
    public static int RealModulo(int devidant, int devisor)
    {
        int a = devidant % devisor;
        return a < 0 ? a + devisor : a;
    }

    public static float RealModulo(float devidant, float devisor)
    {
        float a = devidant % devisor;
        return a < 0 ? a + devisor : a;
    }

    /// <summary>
    /// Gets how close value is from min to max, if x == min than 0, if x== max than 1.
    /// </summary>
    /// <param name="x">value to be calculated</param>
    /// <param name="min">minimum value</param>
    /// <param name="max">maximum value</param>
    /// <returns></returns>
    public static float FromZeroToOne(float x, float min, float max)
    {
        x = Mathf.Clamp(x, min, max);

        return (x - min) / (max - min);
    }
    #endregion

    #region List
    public static T Random<T>(this IEnumerable<T> enumerable)
    {
        int id = UnityEngine.Random.Range(0, enumerable.Count());

        return enumerable.ElementAt(id);
    }

    public static void LeaveNRandom<T>(this List<T> list, int i_newCount)
    {
        while (list.Count > i_newCount)
        {
            list.RemoveAt(UnityEngine.Random.Range(0, list.Count));
        }
    }

    /// <summary>
    /// returns id as if it where carouseled on numbers from 0 to list.count. For example: id = 7, list.count = 3, result is 1. id = -1, count = 2, result is 2.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static int CarouselID<T>(this List<T> list, int id)
    {
        return RealModulo(id, list.Count);
    }

    /// <summary>
    /// returns item at id as if id where carouseled on numbers from 0 to list.count. For example: id = 7, list.count = 3, result is 1. id = -1, count = 2, result is 2.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Carousele<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id)];
    }

    /// <summary>
    /// Returns member on imidiate right of ID, edges work like carousele.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Next<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id + 1)];
    }

    /// <summary>
    /// Returns member on imidiate left of ID, edges work like carousele.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Prev<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id - 1)];
    }

    public static void Shuffle<T>(this List<T> list)
    {
        T a, b;

        int bIndex;

        for (int i = list.Count - 1; i >= 0; i--)
        {
            a = list[i];
            bIndex = UnityEngine.Random.Range(0, list.Count);

            b = list[bIndex];


            list[i] = b;
            list[bIndex] = a;
        }
    }
    #endregion

    #region Vectors
    public static float Random(this Vector2 v) => UnityEngine.Random.Range(v.x, v.y);
    /// <summary>
    /// Inclusive
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static int Random(this Vector2Int v) => UnityEngine.Random.Range(v.x, v.y + 1);

    public static Vector2 Abs(this Vector2 v) => new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Vector3 RotateY(this Vector3 v, float degrees)
    {
        return Quaternion.AngleAxis(degrees, Vector3.up) * v;
    }
    #endregion

    #region GameObject
    public static void Enable(this GameObject g) => g.SetSafeActive(true);

    public static void Disable(this GameObject g) => g.SetSafeActive(false);

    public static void SetSafeActive(this GameObject g, bool value)
    {
        if (g) g.SetActive(value);
    }
    #endregion

    #region Transform
    public static void DestroyAllChildren(this Transform t)
    {
        for (int i = t.childCount - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                GameObject.Destroy(t.GetChild(i).gameObject);
            }
            else
            {
                GameObject.DestroyImmediate(t.GetChild(i).gameObject);
            }
        }
    }

    public static void GainXY(this Transform t, Vector3 i_Position)
    {
        t.position = new Vector3(i_Position.x, i_Position.y, t.position.z);
    }

    public static void GainY(this Transform t, float i_Y) => t.position = new Vector3(t.position.x, i_Y, t.position.z);
    #endregion

    #region Color
    public static Color Transparent(this Color c)
    {
        return new Color(c.r, c.g, c.b, 0f);
    }

    public static Color Opaque(this Color c)
    {
        return new Color(c.r, c.g, c.b, 1f);
    }
    #endregion

    #region Misc
    public static float CashToFloat(this int i) => ((float)(i)) / 100f;

    public static bool RandomBool => UnityEngine.Random.Range(0, 2) == 1;

    public static int ToLayerMask(this int i) => 1 << i;

    #endregion


}
