﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Ducks
{
    public class ProgressController : MonoBehaviour
    {
        private Settings Settings => DucksManager.Instance.Settings;

        private List<ProgressDot> dots = new List<ProgressDot>();

        private void Start()
        {
            ProgressDot activeDot;

            for (int i = 0; i < Settings.ProgressDotAmount; i++)
            {
                activeDot = DucksManager.Instance.pool.Dequeue(PoolType.ProgressDot).GetComponent<ProgressDot>();
                dots.Add(activeDot);
                activeDot.transform.SetParent(transform);
                activeDot.Complete = false;
            }
        }

        public void IncreaseProgress()
        {
            int i = 0;
            for (; i < dots.Count; i++)
            {
                if (!dots[i].Complete)
                {
                    dots[i].Complete = true;
                    break;
                }
            }

            if (i == dots.Count - 1)
            {
                DOVirtual.DelayedCall(Settings.ProgressDotTween.FullTime + 0.1f, () =>
                {
                    dots.ForEach(d => d.Complete = false);
                });
            }
        }
    }
}
