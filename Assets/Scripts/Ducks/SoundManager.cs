﻿using UnityEngine;

namespace Ducks
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private AudioSource SFXSource = null;
        private static SoundManager staticRef => DucksManager.Instance.Sound;

        [SerializeField] private AudioClip m_Wrong = null, m_Correct = null, m_Smoke = null, m_Duck1 = null, m_Duck2 = null, m_water = null;

        public static void PlayWrong()
        {
            Play(staticRef.m_Wrong);
        }
        public static void PlayCorrect()
        {
            Play(staticRef.m_Correct);
        }
        public static void PlaySmoke()
        {
            Play(staticRef.m_Smoke);
        }
        public static void PlayDuck()
        {
            Play(Extentions.RandomBool ? staticRef.m_Duck1 : staticRef.m_Duck2);
        }
        public static void PlayWater()
        {
            Play(staticRef.m_water);
        }

        public static void Play(AudioClip clip)
        {
            staticRef.SFXSource.PlayOneShot(clip);
        }

        private void Reset()
        {
            SFXSource = GetComponentInChildren<AudioSource>();
        }
    }
}
