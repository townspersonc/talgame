﻿using DG.Tweening;
using UnityEngine;

namespace Ducks
{
    public class Bucket : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer bucketRenderer = null;
        public static DucksSettings Params => DucksManager.Instance.Settings.DuckSettings;

        public void Init(Vector3 position, Color color, float spawnDelay)
        {
            transform.position = position;
            bucketRenderer.color = color;

            Appear(spawnDelay);
        }

        public void Appear(float delay)
        {
            transform.localScale = Vector3.zero;

            transform.DOScale(1f, Params.BucketAppearTween.Time)
                .OnStart(() => Smoke.SpawnAt(transform.position))
                .SetEase(Params.BucketAppearTween.Ease)
                .SetDelay(delay);
        }

        public void Hide()
        {
            Smoke.SpawnAt(transform.position);
            transform.DOScale(0f, Params.BucketHideTween.Time).SetEase(Params.BucketHideTween.Ease).OnComplete(() => DucksManager.Instance.pool.Queue(PoolType.Bucket, gameObject));
        }
    }
}
