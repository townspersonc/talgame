﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;

namespace Ducks
{
    public class Duck : MonoBehaviour
    {
        [SerializeField] private SortingGroup sortingGroup = null;
        [SerializeField] private SpriteRenderer bodyRenderer = null;
        [SerializeField] private Transform shadow = null;
        [SerializeField] private BoxCollider2D boxCollider = null;
        public static DucksSettings Params => DucksManager.Instance.Settings.DuckSettings;

        private Tween scaleTween, moveTween, shadowTween;

        private bool dragMode;
        private Vector3 startPos;
        private Bucket bucket;

        public bool Snapped { get; private set; }

        public void Init(Vector3 position, float appearDelay, Bucket bucket, Color color)
        {
            startPos = position;
            this.bucket = bucket;

            bodyRenderer.color = color;
            boxCollider.enabled = true;

            Appear(position, appearDelay);
            dragMode = false;
            Snapped = false;
        }

        private void OnMouseDown()
        {
            EnterDragMode();
        }

        private void OnMouseUp()
        {
            ExitDragMode();
        }

        private void Update()
        {
            DragLoop();
        }
        private void EnterDragMode()
        {
            if (dragMode) return;

            if (scaleTween.IsActive()) scaleTween.Kill();
            if (shadowTween.IsActive()) shadowTween.Kill();

            SoundManager.PlayDuck();

            scaleTween = transform.DOScale(Params.DuckDragEnterTween.Value, Params.DuckDragEnterTween.Time).SetEase(Params.DuckDragEnterTween.Ease);
            shadowTween = shadow.DOScale(0f, Params.ShadowTween.Out.Time).SetEase(Params.ShadowTween.Out.Ease);
            sortingGroup.sortingOrder++;
            dragMode = true;
        }

        private void DragLoop()
        {
            if (!dragMode) return;

            transform.GainXY(Vector3.MoveTowards(transform.position, DucksManager.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition), Params.MoveSpeed * Time.deltaTime));
        }

        private void ExitDragMode()
        {
            if (!dragMode) return;

            if (scaleTween.IsActive()) scaleTween.Kill();
            if (moveTween.IsActive()) moveTween.Kill();
            if (shadowTween.IsActive()) shadowTween.Kill();

            sortingGroup.sortingOrder--;

            if (Vector2.Distance(transform.position, bucket.transform.position) < Params.SnapDistance)
            {
                boxCollider.enabled = false;
                moveTween = transform.DOMove(bucket.transform.position, Params.SnapMoveTween.Time).SetEase(Params.SnapMoveTween.Ease).OnComplete(() =>
                {
                    SoundManager.PlayCorrect();
                    CorrectParticle.SpawnAt(bucket.transform.position);
                    Snapped = true;
                    DucksManager.Instance.OnDuckSnap();
                }
                );
                scaleTween = transform.DOScale(Params.SnapScaleTween.Value, Params.SnapScaleTween.Time).SetEase(Params.SnapScaleTween.Ease);
            }
            else
            {
                moveTween = transform.DOMove(startPos, Params.DuckDragExitMoveTween.Time).SetEase(Params.DuckDragExitMoveTween.Ease).OnComplete(SoundManager.PlayWrong);
                scaleTween = transform.DOScale(1f, Params.DuckDragExitScaleTween.Time).SetEase(Params.DuckDragExitScaleTween.Ease);
            }


            shadowTween = shadow.DOScale(1f, Params.ShadowTween.In.Time).SetEase(Params.ShadowTween.In.Ease);

            dragMode = false;
        }

        public void Hide()
        {
            if (scaleTween.IsActive()) scaleTween.Kill();

            scaleTween = transform.DOScale(0f, Params.DuckHideTween.Time).SetEase(Params.DuckHideTween.Ease).OnComplete(() => DucksManager.Instance.pool.Queue(PoolType.Duck, gameObject));
        }

        public void Appear(Vector3 position, float appearDelay)
        {
            if (scaleTween.IsActive()) scaleTween.Kill();

            transform.position = position;
            transform.localScale = Vector3.zero;

            scaleTween = transform.DOScale(1f, Params.DuckAppearTween.Time)
                .SetEase(Params.DuckAppearTween.Ease)
                .OnStart(SoundManager.PlayWater)
                .SetDelay(appearDelay);
        }

        private void Reset()
        {
            sortingGroup = GetComponentInChildren<SortingGroup>();
            boxCollider = GetComponent<BoxCollider2D>();
        }
    }
}