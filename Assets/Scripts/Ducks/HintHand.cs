﻿using DG.Tweening;
using UnityEngine;
using static Ducks.DucksManager;

namespace Ducks
{
    public class HintHand : MonoBehaviour
    {
        private static HintHandSettings settings => DucksManager.Instance.Settings.HintHandSettings;

        [SerializeField] private SpriteRenderer m_Renderer = null;

        private float startTime;

        private Sequence seq;

        private DuckBucketColorRef activeRef;

        private void Start()
        {
            m_Renderer.color = m_Renderer.color.Transparent();
            startTime = Time.time + settings.StartTime;
        }

        private void Update()
        {
            if (Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                startTime = Time.time + settings.StartTime;
                
                if(seq.IsActive())
                {
                    seq.Kill();
                }
            }

            if(startTime < Time.time && !seq.IsActive())
            {
                activeRef = DucksManager.Instance.GetInCompleteCoupleRef();

                if(activeRef != null)
                {
                    seq = DOTween.Sequence();

                    seq.Append(m_Renderer.DOFade(1f, settings.FadeTween.In.Time).SetEase(settings.FadeTween.In.Ease)).OnStart(() => transform.GainXY(activeRef.duck.transform.position));
                    seq.AppendInterval(settings.FadeTween.Value);
                    seq.Append(DOVirtual.Float(0f, 1f, settings.MoveTime, (i) => transform.GainXY(Vector3.Lerp(activeRef.duck.transform.position, activeRef.bucket.transform.position, i))));
                    seq.AppendInterval(settings.FadeTween.Value);
                    seq.Append(m_Renderer.DOFade(0f, settings.FadeTween.Out.Time).SetEase(settings.FadeTween.Out.Ease));
                    seq.AppendInterval(settings.FadeTween.Value);
                    seq.SetLoops(-1, LoopType.Restart);
                    seq.OnKill(() => m_Renderer.DOFade(0f, settings.FadeTween.Out.Time).SetEase(settings.FadeTween.Out.Ease));
                }
                else
                {
                    startTime = Time.time + settings.StartTime;
                }
            }
        }

        private void Reset()
        {
            m_Renderer = GetComponent<SpriteRenderer>();
        }
    }

    [System.Serializable]
    public class HintHandSettings
    {
        public float StartTime = 8f;

        public TweenDataFloatInOut FadeTween;
        public float MoveTime = 2f;
    }
}
