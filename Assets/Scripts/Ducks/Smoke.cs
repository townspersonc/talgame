﻿using UnityEngine;

namespace Ducks
{
    public class Smoke : MonoBehaviour
    {
        [SerializeField] private Animator m_Animator = null;

        public static void SpawnAt(Vector3 pos)
        {
            DucksManager.Instance.pool.Dequeue(PoolType.Smoke).GetComponent<Smoke>().Init(pos);
        }

        private void Init(Vector3 pos)
        {
            transform.GainXY(pos);
            m_Animator.Rebind();
            SoundManager.PlaySmoke();
        }

        private void OnEnd()
        {
            DucksManager.Instance.pool.Queue(PoolType.Smoke, gameObject);
        }

        private void Reset()
        {
            m_Animator = GetComponent<Animator>();
        }
    }
}
