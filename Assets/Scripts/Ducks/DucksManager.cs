﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ducks
{
    public class DucksManager : Singleton<DucksManager>
    {
        public PoolManager pool = null;
        public Camera mainCamera = null;
        public SoundManager Sound = null;
        [SerializeField] ProgressController progress = null;


        public Settings Settings;
        private DucksSettings dSettings => Settings.DuckSettings;

        public List<DuckBucketColorRef> refs = new List<DuckBucketColorRef>();

        private void Start()
        {
            AppearTween();
        }

        private void AppearTween()
        {
            Vector3 spawnPos = dSettings.BucketSpawnPosition.position;
            spawnPos.x -= dSettings.BucketSpawnWidth / 2f;

            float spawnXOffset = dSettings.BucketSpawnWidth / (dSettings.Amount - 1);

            refs.Clear();

            List<Color> activeColors = new List<Color>(dSettings.colors);
            activeColors.LeaveNRandom(dSettings.Amount);
            activeColors.Shuffle();

            DuckBucketColorRef r = null;
            for (int i = 0; i < dSettings.Amount; i++)
            {
                var bucket = pool.Dequeue(PoolType.Bucket).GetComponent<Bucket>();

                r = new DuckBucketColorRef();
                r.bucket = bucket;
                r.color = activeColors[i];
                refs.Add(r);

                bucket.Init(new Vector3(spawnPos.x + spawnXOffset * i, spawnPos.y + dSettings.BucketCurve.Evaluate(Extentions.FromZeroToOne(i, 0f, dSettings.Amount - 1)), spawnPos.z),
                    activeColors[i],
                    i * dSettings.BucketAppearTween.Value);
            }

            spawnPos = dSettings.DuckSpawnPosition.position;
            spawnPos.x -= dSettings.DuckSpawnWidth / 2f;

            spawnXOffset = dSettings.DuckSpawnWidth / (dSettings.Amount - 1);

            float minStartDelay = dSettings.Amount * dSettings.BucketAppearTween.Value;

            activeColors.Shuffle();

            for (int i = 0; i < dSettings.Amount; i++)
            {
                var duck = pool.Dequeue(PoolType.Duck).GetComponent<Duck>();

                r = refs.First(re => re.color == activeColors[i]);

                r.duck = duck;

                duck.Init(new Vector3(spawnPos.x + spawnXOffset * i, spawnPos.y + dSettings.DuckCurve.Evaluate(Extentions.FromZeroToOne(i, 0f, dSettings.Amount - 1)), spawnPos.z),
                    minStartDelay + dSettings.DuckAppearTween.Value * i,
                    r.bucket,
                    r.color);
            }
        }

        public DuckBucketColorRef GetInCompleteCoupleRef() => refs.FirstOrDefault(r => !r.duck.Snapped);

        public void OnDuckSnap()
        {
            if (refs.Any(r => !r.duck.Snapped)) return;

            foreach (var i in refs)
            {
                i.duck.Hide();
                i.bucket.Hide();
            }

            progress.IncreaseProgress();

            DOVirtual.DelayedCall(Mathf.Max(dSettings.BucketHideTween.Time, dSettings.DuckHideTween.Time) + Settings.RestartDelay, AppearTween);
        }

        private void Reset()
        {
            pool = FindObjectOfType<PoolManager>();
            mainCamera = Camera.main;
            progress = FindObjectOfType<ProgressController>();
            Sound = FindObjectOfType<SoundManager>();
        }

        private class SpawnInfo
        {
            public Vector3 Position = Vector3.zero;
            public float Delay = 0f;
        }

        [System.Serializable]
        public class DuckBucketColorRef
        {
            public Duck duck;
            public Bucket bucket;
            public Color color;
        }
    }

    [System.Serializable]
    public class Settings
    {
        public DucksSettings DuckSettings;
        public float RestartDelay = 1f;

        public TweenDataFloatInOut ProgressDotTween;
        public int ProgressDotAmount = 5;

        public HintHandSettings HintHandSettings;
    }

    [System.Serializable]
    public class DucksSettings
    {
        public int Amount = 3;
        public List<Color> colors = new List<Color>();
        public Transform DuckSpawnPosition, BucketSpawnPosition;
        public float DuckSpawnWidth = 5f, BucketSpawnWidth = 6f;
        public AnimationCurve DuckCurve, BucketCurve;
        public FloatTweenData BucketAppearTween, DuckAppearTween;
        public FloatTweenData DuckDragEnterTween;
        public TweenDataBase DuckDragExitScaleTween;
        public TweenDataBase DuckDragExitMoveTween;
        public float MoveSpeed = 1f;
        public float SnapDistance = 0.5f;
        public TweenDataBase SnapMoveTween;
        public FloatTweenData SnapScaleTween;
        public TweenDataBase DuckHideTween, BucketHideTween;
        public TweenDataInOut ShadowTween;
    }
}