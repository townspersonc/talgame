﻿using UnityEngine;

namespace Ducks
{
    public class CorrectParticle : MonoBehaviour
    {
        [SerializeField] private ParticleSystem m_Particle = null;

        public static void SpawnAt(Vector3 pos)
        {
            DucksManager.Instance.pool.Dequeue(PoolType.CorrectParticle).GetComponent<CorrectParticle>().Init(pos);
        }

        private void Init(Vector3 pos)
        {
            transform.GainXY(pos);
            m_Particle.Play();
        }

        private void OnParticleSystemStopped()
        {
            DucksManager.Instance.pool.Queue(PoolType.CorrectParticle, gameObject);
        }

        private void Reset()
        {
            m_Particle = GetComponent<ParticleSystem>();
        }
    }
}
