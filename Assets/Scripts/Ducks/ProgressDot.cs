﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Ducks
{
    public class ProgressDot : MonoBehaviour
    {
        [SerializeField] private Image image = null;
        [SerializeField] private Sprite completeSprite = null, notCompleteSprite = null;

        private static TweenDataFloatInOut tweenData => DucksManager.Instance.Settings.ProgressDotTween;

        private Sequence sequence;

        private bool complete = false;
        public bool Complete
        {
            get => complete; 
            set
            {
                complete = value;
                RefreshSprite();
            }
        }

        private void OnValidate()
        {
            RefreshSprite(true);
        }

        public void RefreshSprite(bool instant = false)
        {
            if (instant)
            {
                image.sprite = Complete ? completeSprite : notCompleteSprite;
            }
            else
            {
                if (sequence.IsActive()) sequence.Kill();

                sequence = DOTween.Sequence();

                sequence.Append(transform.DOScale(tweenData.Value, tweenData.In.Time).SetEase(tweenData.In.Ease));
                sequence.AppendCallback(() => RefreshSprite(true));
                sequence.Append(transform.DOScale(1f, tweenData.Out.Time).SetEase(tweenData.Out.Ease));
            }
        }


        private void Reset()
        {
            image = GetComponent<Image>();
        }
    }
}